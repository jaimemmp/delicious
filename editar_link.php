<?php
/*
* editar_link.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('inc/funciones.php');
include('clases/Usuario.php');

$usuario = new Usuario($_SESSION["id"]);
$link = $usuario->cargarLink($_GET["id"]);
$relaciones = $usuario->cargarRelaciones($_GET["id"]);
$tags = $usuario->cargarTags();

//elimina de la lista de tags para añadir los que ya están añadidos
foreach($relaciones as $rel){
	foreach($tags as $i => $tag){
		if($rel["id_tag"] == $tag["id"]){
			unset($tags[$i]);
		}
	}
}
?>

<?php include('cabecera.php'); ?>
<?php include('menu.php'); ?>
<div id="wrapper">
	<br/>
	<?php
		echo pintarAlerts();
	?>
	<form method="post" action="guardar_link.php">
		<input type="hidden" name="id" value="<?php echo $link["id"];?>"/>
		<input type="hidden" name="propietario" value="<?php echo $link["propietario"];?>"/>
		<label>nombre:</label><input type="text" name="nombre" value="<?php echo $link["nombre"];?>"/><br/>
		<label>url:</label><input type="text" name="url" value="<?php echo $link["url"];?>"/><br/>
		<label>Publico:</label> <input type="checkbox" name="publico" value="1" <?php if($link["publico"]==1){echo "checked";}?>/>
		<br/>
		<button type="submit" class="btn">Guardar cambios</button>
	</form>
	
	<div id="tags">
		<form class="form-inline" method="post" action="add_rel.php">
			<input type="hidden" name="idlink" value="<?php echo $link["id"];?>">
			<select name="idtag">
				<?php
					foreach($tags as $tag){
						echo '<option value="'.$tag["id"].'">'.$tag["nombre"].'</option>';
					}
				?>
			</select>
			<button type="submit" class="btn btn-primary inlinebutton"><i class="icon-tag icon-white"></i> Add tag</button>
		
			<br/><br/>
			<?php
				foreach($relaciones as $rel){
					echo '<span class="label tag"><i class="icon-tag icon-white"></i> '.$rel["nombre_tag"].' <a href="borrar_relacion.php?link='.$link["id"].'&rel='.$rel["id_rel"].'" alt="eliminar relación"> <i class="icon-remove icon-white"></i> </a> </span> ';
				}
			?>
		</form>
	</div>

</div>
<?php include('pie.php'); ?>

		