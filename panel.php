<?php
/*
* panel.php
* zona privada del usuario.
*/

include('inc/config.php');
include('inc/seguridad.php');
include('inc/funciones.php');
include('clases/Usuario.php');


$usuario = new Usuario($_SESSION["id"]);
?>

<?php include('cabecera.php'); ?>
<?php include('menu.php'); ?>
<div id="wrapper">
	
	<br/>

	
	<a href="nuevo_link.php" class="btn btn-primary">Add link</a>
	<br/><br/>
	<div class="lista">
	<?php
		echo pintarAlerts();
	?>

		<ul class="listatags">
			<?php
				
				$tags = $usuario->cargarTags();

				foreach($tags as $tag){
					echo '<li><i class="icon-tag"></i><a class="tag" href="panel.php?tag='.$tag["id"].'"> '.$tag["nombre"].'</a></li>';
				}
				echo '<li><a class="tag" href="panel.php"> Sin tags</a></li>';
			?>
		</ul>

		<ol>
			<?php
				
				if(isset($_GET["tag"]) AND trim($_GET["tag"])!=""){
					$links = $usuario->cargarLinks($_GET["tag"]);
					$tag = $usuario->cargarTag($_GET["tag"]);
					echo '<span class="label tag"><i class="icon-tag icon-white"></i> '.$tag["nombre"].' <a href="panel.php" alt="eliminar filtro"> <i class="icon-remove icon-white"></i> </a> </span><br/><br/>';
				}else{
					$links = $usuario->cargarLinks();
				}

				foreach($links as $link){
					if($link["publico"]==1){
						$publico = '<i class="icon-eye-open" alt="público"></i> ';
					}else{
						$publico = '<i class="icon-eye-close" alt="privado"></i> ';
					}
					echo '<li><a class="link" href="'.$link["url"].'" target="_blank"> '.$publico.'  '.$link["nombre"].' ('.$link["url"].')</a> <a href="compartir_link.php?id='.$link["id"].'" class="btn btn-small btn-success">compartir</a> <a href="editar_link.php?id='.$link["id"].'" class="btn btn-small btn-primary">editar</a> <a href="borrar_link.php?id='.$link["id"].'" class="btn btn-small btn-danger">borrar</a></li>';
				}
			?>
		</ol>

	</div>


</div>
<?php include('pie.php'); ?>

		