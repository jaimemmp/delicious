<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8"/>
		<meta description="plataforma de gestion de links"/>
		<meta keywords="favoritos, links, enlaces, compartir, guardar, cloud, nube"/>
		<title>Delicious</title>
		    <!-- Le styles -->
	    <link href="css/bootstrap.css" rel="stylesheet">
	    <style type="text/css">
	      body {
	        padding-top: 60px;
	        padding-bottom: 40px;
	      }
	    </style>
	    <link href="css/bootstrap-responsive.css" rel="stylesheet">

	    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	    <!--[if lt IE 9]>
	      <script src="../assets/js/html5shiv.js"></script>
	    <![endif]-->


		<link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="css/estilos.css">
		<script src="js/jquery-1.10.2.min.js"></script>
		<script src="js/delicious.js"></script>
	</head>
	<body>

		<div id="cabecera">
			<a href="index.php" class="logo">DELICIOUS</a>
			<?php
				//muestra login o logout según el estado del usuario
				if(isset($_SESSION["login"]) AND $_SESSION["login"]=="ok"){
					echo '<a href="logout.php" id="boton_login"><i class="icon-off"></i> logout</a>';
				}else{
					echo '<a href="login.php" id="boton_login"><i class="icon-ok"></i> login</a>';
				}
			?>
			
		</div>