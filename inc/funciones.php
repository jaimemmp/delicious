<?php
/*
* funciones.php
* mi documento de funciones
*/

//recorre un array y por cada elemento vacio devuelve
//su indice y lo guarda en otro array de errores.
//devuelve el array de errores, ya sea vacio si todo el array
//inicial estaba bien como con elementos si alguno estaba vacio
function validar($datos){

	//genera el array de errores vacio
	$errores = array();

	//recorre el array de datos
	foreach($datos as $key => $value){
		if(trim($value)==""){
			//si algun dato está vacío pasa su índice al array de errores como valor
			$errores[] = $key;
		}
	}

	//retorna el array de errores
	return $errores;
}


function pintarAlerts(){

	$alerts = '';

	@session_start();

	if(isset($_SESSION["danger"])){//rojo
		$alerts .= '<div class="alert alert-danger">'.$_SESSION["danger"].'</div>';
		unset($_SESSION["danger"]);
	}
	
	if(isset($_SESSION["info"])){//azul
		$alerts .= '<div class="alert alert-info">'.$_SESSION["info"].'</div>';
		unset($_SESSION["info"]);
	}

	if(isset($_SESSION["success"])){//verde
		$alerts .= '<div class="alert alert-success">'.$_SESSION["success"].'</div>';
		unset($_SESSION["success"]);
	}

	if(isset($_SESSION["warning"])){//amarillo
		$alerts .= '<div class="alert alert-warning">'.$_SESSION["warning"].'</div>';
		unset($_SESSION["warning"]);
	}

	return $alerts;
}
?>