<?php
/*
* borrar_relacion.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('clases/Usuario.php');

$id_rel = $_GET["rel"];
$id_link = $_GET["link"];

$usuario = new Usuario($_SESSION["id"]);

if($usuario->borrarRelacion($id_rel)===true){
	$_SESSION["success"] = 'La relaci&oacute;n se ha eliminado correctamente';
}else{
	$_SESSION["danger"] = 'No se pudo eliminar la relaci&oacute;n';
}

header('location: editar_link.php?id='.$id_link);

?>