<?php
/*
* editar_cuenta.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('inc/funciones.php');
include('clases/Usuario.php');

if(isset($_POST["passactual"]) AND trim($_POST["passactual"])!=""){


	//guarda en un array los datos del formulario
	$recibido = array(
		"nombre" => $_POST["nombre"],
		"apellido" => $_POST["apellido"],
		"nick" => $_POST["nick"],
		"passactual" => $_POST["passactual"],
		"email" => $_POST["email"]
	);

	if(isset($_POST["passnueva"]) AND trim($_POST["passnueva"])!=""){
		$recibido["passnueva"] = $_POST["passnueva"];
	}

	//valida lo recibido y lo mete en errores (array)
	$errores = validar($recibido);

	//cuenta los errores del array $errores
	if(count($errores)>=1){//si hay al menos uno
		
		//genera msg
		$_SESSION["danger"] = 'Has dejado en blanco los siguientes datos que son obligatorios: ';
		$_SESSION["danger"] .= implode(', ', $errores);
		header('Location: cuenta.php');

	}else{//si no hay ninguno

		//instancia la clase Usuario en el objeto $usuario
		$usuario = new Usuario($_SESSION["id"]);
		//llama al método actualizar del objeto $usuario pasando como parámetro los datos del form
		$actualizacion = $usuario->actualizar($recibido);
		if($actualizacion===true){
			$_SESSION["success"] = 'Los cambios se guardaron correctamente.';
			header('Location: cuenta.php');
		}else{
			$_SESSION["danger"] = 'No se pudieron guardar los cambios '.$actualizacion;
			header('Location: cuenta.php');
		}

	}
}else{
	//genera msg y lo manda por get
	$_SESSION["danger"] = 'No ha introducido la contraseña y es obligatoria para realizar cambios.';
	header('Location: cuenta.php');
}

?>