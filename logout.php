<?php
/*
* logout.php
* cierra la sesión de usuario
*/
require_once('inc/config.php');
require_once('clases/Usuario.php');

@session_start();

$usuario = new Usuario($_SESSION["id"]);
$usuario->logout();

header('location: index.php');

?>