<?php
/*
* borrar_link.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('clases/Usuario.php');

$id_link = $_GET["id"];

$usuario = new Usuario($_SESSION["id"]);

if($usuario->borrarLink($id_link)===true){
	$_SESSION["success"] = 'El link se ha eliminado correctamente';
}else{
	$_SESSION["danger"] = 'No se pudo eliminar el link';
}

header('location: panel.php');

?>