<?php
/*
* comprobar_datos.php
*/
require_once('inc/config.php');
require_once('clases/Usuario.php');
require_once('inc/funciones.php');

@session_start();

$recibido = array(
		"nick_email" => $_POST["nick_email"],
		"pass" => $_POST["pass"]
	);

$errores = validar($recibido);

if(count($errores)>=1){
	
	//genera error y lo guarda
	$_SESSION["danger"] = 'Has dejado en blanco los siguientes datos que son obligatorios: '.implode(', ', $errores);
	header('Location: login.php');

}else{

	$usuario = new Usuario();
	$usuario->login($recibido);

	//si la propiedad errores del usuario está vacía envía al panel
	if(count($usuario->errores) == 0){
	
		$_SESSION["success"] = 'Bienvenido '.$usuario->getNombre();
		header('Location: panel.php');

	}else{
		
		$_SESSION["danger"] = 'Se han producido los siguientes errores: '.implode('<br/>- ', $usuario->errores);
		header('Location: login.php');
	
	}

}

?>