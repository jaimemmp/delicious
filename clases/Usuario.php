<?php
/*
* Usuario.php
* Clase Usuario
*/
class Usuario{

	private $id = null;

	private $nombre;
	private $apellido;
	private $email;
	private $nick;
	private $pass;
	private $token;
	private $activo = 0;

	public $info;
	public $errores = array();
	public $msg = array();



	//constructor
	function Usuario($id = null){
		
		//si recibe un entero
		if(is_numeric($id)){

			//actualiza el id con el entero recibido
			$this->id = $id;
			//ejecuta el método de cargar datos
			$this->cargarDatos();

		}else{

			//actualiza propiedad info
			$this->info = utf8_decode('Usuario anónimo.');

		}

	}


	//crea un objeto de conexion a la base de datos
	private function conectarBD(){

		//utiliza una variable que no pertenece al espacio de nombres de variable de la función
		global $db_params;

		//instancia el objeto de la clase mysqli con los datos de la variable externa
		$db = new mysqli($db_params['host'], $db_params['user'], $db_params['pass'], $db_params['db']);

		if($db->connect_errno > 0){
			//si hay error de conexión lo guarda en la propiedad errores
			$this->errores[] = $db->connect_error;
		}else{
			//si no hay error devuelve el objeto de conexión
			return $db;
		}
	}


	//carga los datos del usuario de la bbdd
	private function cargarDatos(){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//genera la consulta
		$sql = 'SELECT * FROM usuarios WHERE id='.$this->id;

		//el objeto de conexión ejecuta la sentencia
		if($result = $db->query($sql)){
			
			//pasa el resultado a la variable row en forma de array asociativo
			$row = $result->fetch_assoc();
			
			//libera la memoria de los resultados sql (ya no los necesita porque están en $row en forma de array)
			$result->free();

			//actualiza la propiedades del usuario con los resultados de la consulta
			$this->nombre = $row["nombre"];
			$this->apellido = $row["apellido"];
			$this->email = $row["email"];
			$this->nick = $row["nick"];
			$this->pass = $row["pass"];
			$this->token = $row["token"];
			$this->activo = $row["activo"];
		
		}else{//si la consulta falla
			
			//actualiza la propiedad errores
			$this->errores[] = $db->error;

			//actualiza la propiedad info del usuario
			$this->info = utf8_decode('Usuario anónimo.');
		
		}

		//cierra la conexión con la bbdd
		$db->close();
	}



	public function login($param){
		
		if(is_array($param)){

			//conecta con la bbdd mediante el objeto de conexión
			$db = $this->conectarBD();

			//genera la consulta para buscar el suuario que tenga el nick o el email igual al recibido
			$sql = 'SELECT * FROM usuarios WHERE
					nick="'.$param["nick_email"].'"
					OR email="'.$param["nick_email"].'"
					';

			//el objeto de conexión ejecuta la sentencia
			if($result = $db->query($sql)){
				
				//compruebo el numero de resultados
				if($result->num_rows>0){
					
					//pasa el resultado a la variable row en forma de array asociativo
					$row = $result->fetch_assoc();
					
					//libera la memoria de los resultados sql (ya no los necesita porque están en $row en forma de array)
					$result->free();

					//comprueba si coincide la contraseña recibido con la de la bbdd
					if($param["pass"] == $row["pass"]){

						//actualiza la propiedades del usuario con los resultados de la consulta
						$this->id = $row["id"];
						$this->nombre = $row["nombre"];
						$this->apellido = $row["apellido"];
						$this->email = $row["email"];
						$this->nick = $row["nick"];
						$this->token = $row["token"];
						$this->activo = $row["activo"];

						//compruebo si está activado
						if($row["activo"]==1){

							$this->iniciarSesion();

							$return = true;

						}else{

							//actualiza la propiedad errores
							$this->errores[] = 'Usuario pendiente de activación. Para activar su cuenta puede ir <a href="confirmar.php?token='.$this->token.'&id='.$this->id.'">aquí</a>';

							//actualiza la propiedad info del usuario
							$this->info = utf8_decode('Usuario amónimo.');

							$return = false;
						}

					//si la contraseña no coincide
					}else{

						//actualiza la propiedad errores
						$this->errores[] = utf8_decode('Contraseña incorrecta.');

						//actualiza la propiedad info del usuario
						$this->info = utf8_decode('Usuario amónimo.');

						$return = false;

					}

				//si no encuentra ningún resultado en la consulta
				//puede ser que no exista
				}else{

					echo $this->id.' '.$this->token;
					//actualiza la propiedad errores
					$this->errores[] = 'Usuario no encontrado.';

					//actualiza la propiedad info del usuario
					$this->info = utf8_decode('Usuario amónimo.');

					$return = false;
				}

			
			}else{//si la consulta falla
				
				//actualiza la propiedad errores
				$this->errores[] = 'Se ha producido un error. Disculpe las molestias.';

				//actualiza la propiedad info del usuario
				$this->info = utf8_decode('Usuario amónimo.');

				$return = false;
			
			}



		}else{
			
			//actualiza la propiedad errores
			$this->errores[] = utf8_decode('Los datos para el login no son adecuados.');
			
			//actualiza la propiedad info del usuario
			$this->info = utf8_decode('Usuario anónimo.');
			
			$return = false;

		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}



	private function iniciarSesion(){
		@session_start();
		$_SESSION["id"] = $this->id;
		$_SESSION["nombre"] = $this->nombre;
		$_SESSION["apellido"] = $this->apellido;
		$_SESSION["email"] = $this->email;
		$_SESSION["nick"] = $this->nick;
		$_SESSION["activo"] = $this->activo;
		$_SESSION["token"] = $this->token;
		$_SESSION["login"] = 'ok';
		return true;
	}




	public function registrar($param){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();
		
		//genera la consulta
		$sql = 'SELECT * FROM usuarios WHERE email="'.$param["email"].'" OR nick="'.$param["email"].'"';

		//el objeto de conexión ejecuta la sentencia
		if($result = $db->query($sql)){

			//si encuantra algún registro es que ya existe un usuario con esos datos
			if($result->num_rows>0){

				//libera la memoria de los resultados sql (ya no los necesita porque están en $row en forma de array)
				$result->free();

				//actualiza la propiedad errores
				$this->errores[] = utf8_decode('El email o el nick que has introducido no están disponibles.');

				//actualiza la propiedad info del usuario
				$this->info = utf8_decode('Usuario anónimo.');

				return false;

			//si no encuentra coincidencias
			}else{

				//genera la consulta
				$sql = 'INSERT INTO
						usuarios (nombre, apellido, email, nick, token, pass)
						VALUES(
							"'.$param["nombre"].'",
							"'.$param["apellido"].'",
							"'.$param["email"].'",
							"'.$param["nick"].'",
							"'.uniqid("", true).'",
							"'.$param["pass"].'"
						)';

				//el objeto de conexión ejecuta la sentencia
				if($result = $db->query($sql)){//si se ejecuta bien es que ya hay alguien registrado con esos datos
					
					//obtiene el id del insert y actualiza el ide del usuario
					$this->id = $db->insert_id;

					//carga los datos del usuario
					$this->cargarDatos();
					
					return true;
				
				}else{//si la consulta falla es que no existe ningún usuario con esos datos y se puede registrar
					
					//actualiza la propiedad errores
					$this->errores[] = $db->error;

					//actualiza la propiedad info del usuario
					$this->info = utf8_decode('Usuario anónimo.');

					return false;
				
				}

			}

			
		
		}else{//si la consulta falla
			
			//actualiza la propiedad errores
			$this->errores[] = utf8_decode('No se ha podido efectuar el regsitro .'.$db->error);

			//actualiza la propiedad info del usuario
			$this->info = utf8_decode('Usuario anónimo.');

			return false;
		
		}

		//cierra la conexión con la bbdd
		$db->close();
	}



	public function confirmar($token){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//genera la consulta
		$sql = 'UPDATE usuarios SET activo=1 WHERE token="'.$token.'"';

		//el objeto de conexión ejecuta la sentencia
		if($result = $db->query($sql)){
			
			if($db->affected_rows == "1"){

				//actualiza la propiedad info del usuario
				$this->info = utf8_decode('Cuenta activada.');

				$return = true;

			}else{

				//actualiza la propiedad errores
				$this->errores[] = utf8_decode('No se pudo realizar la activación.');

				//actualiza la propiedad info del usuario
				$this->info = utf8_decode('Pendiente de activación.');

				$return = false;
			}
			
		
		}else{//si la consulta falla
			
			//actualiza la propiedad errores
			$this->errores[] = $db->error;

			//actualiza la propiedad info del usuario
			$this->info = utf8_decode('Pendiente de activación.');

			$return = false;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;

	}



	public function cargarLinks($tag=null){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//declara la sentencia sql
		if($tag == null){
			$sql = 'SELECT * FROM links WHERE propietario='.$this->id;
		}else{
			$sql = 'SELECT * FROM rel_tags_links rels
					LEFT JOIN links
					ON rels.id_link = links.id
					WHERE links.propietario='.$this->id.' AND rels.id_tag='.$tag;
					//echo $sql;
		}
		//ejecuta sentencia sql
		if($result = $db->query($sql)){

			$links = array();

			//pasa el resultado a la variable row en forma de array asociativo
			while($row = $result->fetch_assoc()){
				$links[] = $row;
			}
			
			//libera la memoria de los resultados sql (ya no los necesita porque están en $row en forma de array)
			$result->free();

			$return = $links;

		}else{
			
			//actualiza la propiedad errores
			$this->errores[] = $db->error;

			//actualiza la propiedad info del usuario
			$this->info = utf8_decode('Usuario anónimo.');

			$return = false;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}


	//comparte un link con una lista de usuarios
	public function compartirLink($id_link, $usuarios){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//cargo los datos del link
		$link = $this->cargarLink($id_link);

		//preparo array de informe
		$informe = array();

		//recorro usuarios
		foreach($usuarios as $nick){
			
			//elimino espacios del nick
			$nick = trim($nick);
			
			//cargo datos del usuario
			$usuario = $this->cargaUsuario($nick);
			
			if($usuario["id"]==0 OR $usuario["id"]==false){
				//agrego indices y valores al array
				$informe[$nick] = 'El usuario no existe';
			}else{
				$sql = 'INSERT INTO links
						(nombre, url, publico, propietario)
						VALUES("'.$link["nombre"].'",
								"'.$link["url"].'",
								"'.$link["publico"].'",
								"'.$usuario["id"].'")';
				
				if($db->query($sql)){

					//agrego indices y valores al array
					$informe[$nick] = $db->insert_id;

				}else{
					
					//agrego indices y valores al array
					$informe[$nick] = $db->error;
				
				}
			}

		}
		
		//cierra la conexión con la bbdd
		$db->close();

		return $informe;
	}

	//carga los datos de un usuario en funcion del nick
	private function cargaUsuario($nick){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//sentencia sql
		$sql = 'SELECT * FROM usuarios WHERE nick="'.$nick.'"';
		//ejecucion de la sentencia
		if($result = $db->query($sql)){
			//transforma el resultado sql en un array asociativo
			$return = $result->fetch_assoc();

		}else{
			//retorna el error mysql
			$return = $db->error;
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}


	//carga los datos de un link en base a su id
	public function cargarLink($id_link){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//sentencia sql
		$sql = 'SELECT * FROM links WHERE id='.$id_link;
		
		//ejecución de la sentencia
		if($result = $db->query($sql)){

			//guarda los datos del link en un array asociativo
			$return = $result->fetch_assoc();

		}else{

			$return = $db->error;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}


	//guarda los datos de un link en la bbdd
	public function guardarLink($param, $nuevo = false){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		if($nuevo == false){

		//sentencia sql
			$sql = 'UPDATE links SET
					nombre="'.$param["nombre"].'", 
					url="'.$param["url"].'", 
					publico="'.$param["publico"].'",
					propietario="'.$param["propietario"].'"
					WHERE id='.$param["id"];
		}else{
			$sql = 'INSERT INTO links (nombre, url, publico, propietario)
					VALUES("'.$param["nombre"].'", "'.$param["url"].'", "'.$param["publico"].'", "'.$param["propietario"].'")';
		}

		//ejecución de la sentencia
		if($db->query($sql)){

			$return = true;
		
		}else{
		
			$return = $db->error;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;

	}


	//borra un link según su id
	public function borrarLink($id_link){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//sentenci sql apra borrar
		$sql = 'DELETE FROM links WHERE id='.$id_link;

		//ejecuta la sentencia sql
		if($db->query($sql)){
			$return = true;
		}else{
			$return = false;
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}


	//borra un tag según su id
	public function borrarTag($id_tag){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//sentencia sql para borrar
		$sql = 'DELETE tags, rel_tags_links FROM tags ,rel_tags_links WHERE tags.id='.$id_tag.' AND rel_tags_links.id_tag='.$id_tag;

		//ejecuta la sentencia sql
		if($db->query($sql)){
			$return = true;
		}else{
			$return = false;
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}




	public function actualizar($param){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		$this->nombre = $param["nombre"];
		$this->apellido = $param["apellido"];
		$this->nick = $param["nick"];
		$this->email = $param["email"];

		if($param["passactual"] == $this->pass){

			if(isset($param["passnueva"])){
				
				$this->pass = $param["passnueva"];

				$sql = 'UPDATE usuarios SET
					nombre = "'.$this->nombre.'",
					apellido = "'.$this->apellido.'",
					nick = "'.$this->nick.'",
					email = "'.$this->email.'",
					pass = "'.$this->passnueva.'"
					WHERE id='.$this->id;
			}else{
				$sql = 'UPDATE usuarios SET
					nombre = "'.$this->nombre.'",
					apellido = "'.$this->apellido.'",
					nick = "'.$this->nick.'",
					email = "'.$this->email.'"
					WHERE id='.$this->id;
			}

			//ejecuta la sentencia sql
			if($db->query($sql)){
				$return = true;
			}else{
				$this->errores[] = $db->error;
				$return = false;
			}

		}else{

			$this->errores[] = 'La contraseña actual no es correcta.';
			$return = false;
			
		}
		//cierra la conexión con la bbdd
		$db->close();

		return $return;

	}






	public function logout(){
		@session_start();
		unset($_SESSION);
		session_destroy();
		return true;
	}



	public function baja(){
		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//sentenci sql para borrar
		$sql = 'DELETE FROM usuarios WHERE id='.$this->id;

		//ejecuta la sentencia sql
		if($db->query($sql)){
			
			$sql = 'DELETE FROM links WHERE propietario='.$this->id;

			if($db->query($sql)){
				
				$return = true;

			}else{

				$return = false;

			}

		}else{
			$return = false;
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}


	public function cargarTags(){
		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//declara la sentencia sql
		$sql = 'SELECT * FROM tags WHERE propietario='.$this->id;
		
		//ejecuta sentencia sql
		if($result = $db->query($sql)){

			$tags = array();

			//pasa el resultado a la variable row en forma de array asociativo
			while($row = $result->fetch_assoc()){
				$tags[] = $row;
			}
			
			//libera la memoria de los resultados sql (ya no los necesita porque están en $row en forma de array)
			$result->free();

			$return = $tags;

		}else{
			
			//actualiza la propiedad errores
			$this->errores[] = $db->error;

			//actualiza la propiedad info del usuario
			$this->info = utf8_decode('Usuario anónimo.');

			$return = false;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}

	//carga los datos de un tag en base a su id
	public function cargarTag($id_tag){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//sentencia sql
		$sql = 'SELECT * FROM tags WHERE id='.$id_tag;
		
		//ejecución de la sentencia
		if($result = $db->query($sql)){

			//guarda los datos del tag en un array asociativo
			$return = $result->fetch_assoc();

		}else{

			$return = $db->error;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}



	//guarda los datos de un tag en la bbdd
	public function guardarTag($param, $nuevo = false){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		if($nuevo == false){

		//sentencia sql
			$sql = 'UPDATE tags SET
					nombre="'.$param["nombre"].'"
					WHERE id='.$param["id"];
		}else{
			$sql = 'INSERT INTO tags (nombre, propietario)
					VALUES("'.$param["nombre"].'", "'.$param["propietario"].'")';
		}

		//ejecución de la sentencia
		if($db->query($sql)){

			$return = true;
		
		}else{
		
			$return = $db->error;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;

	}



	public function cargarRelaciones($id_link){
		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//declara la sentencia sql
		$sql = 'SELECT * FROM rel_tags_links
				LEFT JOIN tags
				ON rel_tags_links.id_tag = tags.id
				WHERE rel_tags_links.id_link='.$id_link;
		
		//ejecuta sentencia sql
		if($result = $db->query($sql)){

			$tags = array();

			//pasa el resultado a la variable row en forma de array asociativo
			while($row = $result->fetch_assoc()){
				$tags[] = array("id_rel" => $row["id_rel"], "id_tag" => $row["id_tag"], "nombre_tag" => $row["nombre"]);
			}
			
			//libera la memoria de los resultados sql (ya no los necesita porque están en $row en forma de array)
			$result->free();

			$return = $tags;

		}else{
			
			//actualiza la propiedad errores
			$this->errores[] = $db->error;

			$return = false;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}



	public function borrarRelacion($id_rel){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		//sentencia sql para borrar
		$sql = 'DELETE FROM rel_tags_links WHERE id_rel='.$id_rel;

		//ejecuta la sentencia sql
		if($db->query($sql)){
			$return = true;
		}else{
			$return = false;
			//actualiza la propiedad errores
			$this->errores[] = $db->error;
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;
	}


	public function guardarRel($param, $nuevo = false){

		//conecta con la bbdd mediante el objeto de conexión
		$db = $this->conectarBD();

		if($nuevo == false){
			//sentencia sql
			$sql = 'UPDATE rel_tags_links SET
					id_link="'.$param["idlink"].'",
					id_tag="'.$param["idtag"].'"
					WHERE id_rel='.$param["idrel"];
		}else{
			$sql = 'INSERT INTO rel_tags_links (id_link, id_tag)
					VALUES("'.$param["idlink"].'", "'.$param["idtag"].'")';
		}

		//ejecución de la sentencia
		if($db->query($sql)){

			$return = true;
		
		}else{
		
			$return = $db->error;
		
		}

		//cierra la conexión con la bbdd
		$db->close();

		return $return;

	}



	/*GETTERS*/

	
	//devuelve el id del usuario
	public function getId(){
		return $this->id;
	}

	//devuelve el nombre del usuario
	public function getNombre(){
		return $this->nombre;
	}

	//devuelve el apellido del usuario
	public function getApellido(){
		return $this->apellido;
	}

	//devuelve el email del usuario
	public function getEmail(){
		return $this->email;
	}

	//devuelve el nick del usuario
	public function getNick(){
		return $this->nick;
	}

	//devuelve el token del usuario
	public function getToken(){
		return $this->token;
	}

	//devuelve el estado del usuario
	public function getActivo(){
		return $this->activo;
	}

	//devuelve el info del usuario
	public function getInfo(){
		return $this->info;
	}




	/*SETTERS*/

	//actualiza el id del usuario
	// No se debe poder cambiar el id
	// }

	//actualiza el nombre del usuario
	public function setNombre($param){
		if($this->nombre = $param){
			return true;
		}else{
			return false;
		}
	}

	//actualiza el apellido del usuario
	public function setApellido($param){
		if($this->apellido = $param){
			return true;
		}else{
			return false;
		}
	}

	//actualiza el email del usuario
	public function setEmail($param){
		if($this->email = $param){
			return true;
		}else{
			return false;
		}
	}

	//actualiza el email del usuario
	public function setNick($param){
		if($this->nick = $param){
			return true;
		}else{
			return false;
		}
	}

	//actualiza el token del usuario
	public function setToken($param){
		if($this->token = $param){
			return true;
		}else{
			return false;
		}
	}

	//actualiza el estado del usuario
	public function setActivo($param){
		if($this->activo = $param){
			return true;
		}else{
			return false;
		}
	}

	//actualiza el info del usuario
	public function setInfo($param){
		if($this->info = $param){
			return true;
		}else{
			return false;
		}
	}

	
}
?>