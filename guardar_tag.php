<?php
/*
* guardar_tag.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('inc/funciones.php');
include('clases/Usuario.php');

//define un array con los datos del form
if(!isset($_POST["publico"])){
	$_POST["publico"]="0";
}

$param = array(
	"id" => $_POST["id"],
	"nombre" => $_POST["nombre"]
);

$usuario = new Usuario($_SESSION["id"]);

//compruebo si los datos del tag se guardan bien
if($usuario->guardarTag($param, false)===true){
	//si se guardan bien preparo un success
	$_SESSION["success"] = 'Los cambios se han guardado correctamente';
}else{
	//si NO se guardan bien preparo un error
	$_SESSION["danger"] = 'No se han podido guardar los cambios';
}

header('location: editar_tag.php?id='.$_POST["id"]);

?>