<?php
// Varios destinatarios
$para  = 'aidan@example.com' . ', '; // atención a la coma
$para .= 'jmmgbu@gmail.com';

// subject
$titulo = 'Recordatorio de cumpleaños para Agosto';

// message
$mensaje = '
<html>
<head>
  <title>Recordatorio de cumpleaños para Agosto</title>
</head>
<body>
  <p>¡Estos son los cumpleaños para Agosto!</p>
  <table>
    <tr>
      <th>Quien</th><th>Día</th><th>Mes</th><th>Año</th>
    </tr>
    <tr>
      <td>Joe</td><td>3</td><td>Agosto</td><td>1970</td>
    </tr>
    <tr>
      <td>Sally</td><td>17</td><td>Agosto</td><td>1973</td>
    </tr>
  </table>
</body>
</html>
';

// Para enviar un correo HTML mail, la cabecera Content-type debe fijarse
$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Cabeceras adicionales
$cabeceras .= 'To: Juanito <jmmgbu@gmail.com>,  <jmmgb@wanadoo.es>' . "\r\n";
$cabeceras .= 'From: paquin <jmoreno117@hotmail.es>' . "\r\n";
$cabeceras .= 'Cc: ' . "\r\n";
$cabeceras .= 'Bcc: ' . "\r\n";

// Mail it
mail($para, $titulo, $mensaje, $cabeceras);
?>