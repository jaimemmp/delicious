<?php
/*
* add_rel.php
* recibe los datos del formulario de añadir rel 
*/

include('inc/seguridad.php');
include('inc/config.php');
include('inc/funciones.php');
include('clases/Usuario.php');

//instancia la clase Usuario en el objeto $usuario
$usuario = new Usuario($_SESSION["id"]);

//comprueba si el pass y el repass son iguales

//guarda en un array los datos del formulario
$recibido = array(
	"idlink" => $_POST["idlink"],
	"idtag" => $_POST["idtag"]
);

//valida lo recibido y lo mete en errores (array)
$errores = validar($recibido);

//cuenta los errores del array $errores
if(count($errores)>=1){//si hay al menos uno
	
	//genera msg y lo manda
	$_SESSION["danger"] = 'Has dejado en blanco los siguientes datos que son obligatorios: ';
	$_SESSION["danger"] .= implode(', ', $errores);
	header('Location: editar_link.php?id='.$recibido["idlink"]);

}else{//si no hay ninguno

	if($usuario->guardarRel($recibido, true) === true){
		$_SESSION["success"] = 'Nueva relaci&oacute;n creada';
		header('Location: editar_link.php?id='.$recibido["idlink"]);
	}else{
		$_SESSION["danger"] = 'Se han producido los siguientes errores: ';
		$_SESSION["danger"] .= implode(', ', $usuario->errores);
		header('Location: editar_link.php?id='.$recibido["idlink"]);
	}
}



?>