<?php

require_once('inc/config.php');
require_once('clases/Usuario.php');
require_once('inc/funciones.php');

@session_start();

//comprueba si el pass y el repass son iguales
if($_POST["pass"]==$_POST["repass"]){
	
	//guarda en un array los datos del formulario
	$recibido = array(
		"nombre" => $_POST["nombre"],
		"apellido" => $_POST["apellido"],
		"nick" => $_POST["nick"],
		"pass" => $_POST["pass"],
		"email" => $_POST["email"]
	);

	//valida lo recibido y lo mete en errores (array)
	$errores = validar($recibido);

	//cuenta los errores del array $errores
	if(count($errores)>=1){//si hay al menos uno

		//genera error y lo guarda
		$_SESSION["danger"] = 'Has dejado en blanco los siguientes datos que son obligatorios: '.implode(', ', $errores);

	}else{//si no hay ninguno

		$usuario = new Usuario();
		if( $usuario->registrar($recibido) ){
			$_SESSION["success"] = 'Su cuenta ya está registrada.<br/>
									Puede activar su cuenta pinchando
									<a href="confirmar.php?token='.$usuario->getToken().'"><strong>aquí</strong></a>';
		}else{
			$_SESSION["danger"] = 'No se ha podido registrar el usuario';
		}

	}

}else{//si no coinciden las contraseñas
	//genera error y lo guarda
	$_SESSION["danger"] = 'Las contraseñas no coinciden. Deben ser idénticas.';
}

header('Location: index.php');

?>