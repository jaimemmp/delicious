<?php
/*
* borrar_tag.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('clases/Usuario.php');

$id_tag = $_GET["id"];

$usuario = new Usuario($_SESSION["id"]);

if($usuario->borrarTag($id_tag)===true){
	$_SESSION["success"] = 'El tag se ha eliminado correctamente';
}else{
	$_SESSION["danger"] = 'No se pudo eliminar el tag';
}

header('location: tags.php');

?>