<?php
/*
* cuenta.php
* edita los datos de la cuenta.
*/

include('inc/seguridad.php');
include('inc/config.php');
include('inc/funciones.php');
include('clases/Usuario.php');


$usuario = new Usuario($_SESSION["id"]);

?>

<?php include('cabecera.php'); ?>
<?php include('menu.php'); ?>
<div id="wrapper">
	
	<br/>
	<?php
		echo pintarAlerts();
	?>
	<form method="post" action="editar_cuenta.php">
		<input type="hidden" name="id" value="<?php echo $usuario->getId();?>"/>
		<label>nombre:</label> <input type="text" name="nombre" value="<?php echo $usuario->getNombre();?>"/>
		<label>apellido:</label> <input type="text" name="apellido" value="<?php echo $usuario->getApellido();?>"/><br/>
		<label>nick:</label> <input type="text" name="nick" value="<?php echo $usuario->getNick();?>"/>
		<label>email:</label> <input type="text" name="email" value="<?php echo $usuario->getEmail();?>"/><br/>
		<label>pass actual:</label> <input type="text" name="passactual" value=""/>
		<label>pass nueva:</label> <input type="text" name="passnueva" value=""/>
		<br/>
		<button type="submit" class="btn">Guardar cambios</button>
	</form>
	
	<a href="baja.php" class="btn btn-big btn-danger">Darse de baja. Se borrarán todos sus links</a>


</div>
<?php include('pie.php'); ?>

		