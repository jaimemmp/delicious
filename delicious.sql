-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 06-09-2013 a las 11:58:55
-- Versión del servidor: 5.5.25
-- Versión de PHP: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `delicious`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `links`
--

CREATE TABLE `links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '',
  `propietario` int(11) NOT NULL,
  `publico` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Volcado de datos para la tabla `links`
--

INSERT INTO `links` (`id`, `nombre`, `url`, `propietario`, `publico`) VALUES
(1, 'google', 'http://www.google.es', 4, 1),
(2, 'google', 'http://www.google.es', 4, 1),
(5, 'marca', 'http://www.marca.com', 1, 1),
(6, 'as', 'http://www.as.com', 1, 0),
(8, 'google', 'http://www.google.es', 1, 1),
(9, 'gmail', 'http://www.gmail.com', 1, 0),
(12, 'gmail', 'http://www.gmail.com', 2, 1),
(13, 'gmail', 'http://www.gmail.com', 4, 1),
(14, 'unlink', 'http://unlink.com', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rel_tags_links`
--

CREATE TABLE `rel_tags_links` (
  `id_rel` int(11) NOT NULL AUTO_INCREMENT,
  `id_tag` int(11) DEFAULT NULL,
  `id_link` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `rel_tags_links`
--

INSERT INTO `rel_tags_links` (`id_rel`, `id_tag`, `id_link`) VALUES
(1, 1, 6),
(2, 1, 5),
(3, 2, 8),
(4, 2, 9),
(5, 4, 6),
(7, 4, 8),
(8, 4, 9),
(9, 5, 4),
(10, 4, 5),
(11, 2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `propietario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tags`
--

INSERT INTO `tags` (`id`, `nombre`, `propietario`) VALUES
(1, 'periodicos', 1),
(2, 'utilidades', 1),
(4, 'varios', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nick` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `pass` varchar(255) NOT NULL DEFAULT '',
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(255) NOT NULL DEFAULT '',
  `nombre` varchar(255) NOT NULL DEFAULT '',
  `apellido` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nick` (`nick`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nick`, `email`, `pass`, `activo`, `token`, `nombre`, `apellido`) VALUES
(1, 'jaimemmp', 'jaimemmp@gmail.com', '1234', 1, '52123271c31b67.95399221', 'jaime', 'muÃ±oz'),
(2, 'pepito', 'pepito@gmail.com', '1234', 1, '521c7ed65b9d96.88733581', 'pepito', 'pepito'),
(4, 'er', 'er', 'er', 1, '521c90cd89e709.12606675', 'er', 'er');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
