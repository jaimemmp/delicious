<?php
/*
* confirmar_baja.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('clases/Usuario.php');


$usuario = new Usuario($_SESSION["id"]);
$usuario->baja();
$usuario->logout();

header('location: index.php');

?>