<?php
/*
* tags.php
*/

include('inc/config.php');
include('inc/seguridad.php');
include('inc/funciones.php');
include('clases/Usuario.php');


$usuario = new Usuario($_SESSION["id"]);
?>

<?php include('cabecera.php'); ?>
<?php include('menu.php'); ?>
<div id="wrapper">
	
	<?php
		echo pintarAlerts();
	?>
	<form class="form-inline" method="post" action="add_tag.php">
		<input type="hidden" name="propietario" value="<?php echo $_SESSION["id"];?>"/>
		<input type="text" name="nombre" placeholder="nombre del tag" value="">
		<button type="submit" class="btn btn-primary inlinebutton"><i class="icon-tag icon-white"></i> Crear tag</button>
	</form>

	
	<div class="lista">
	

		<ul class="mistags">
			<?php
				
				$tags = $usuario->cargarTags();

				foreach($tags as $tag){
					echo '
						<li>
							<i class="icon-tag"></i>
							<a class="tag" href="panel.php?tag='.$tag["id"].'"> '.$tag["nombre"].' </a>
							<a href="editar_tag.php?id='.$tag["id"].'"> <i class="icon-pencil"></i> </a> 
							<a href="borrar_tag.php?id='.$tag["id"].'"> <i class="icon-remove"></i> </a>
						</li>';
				}
			?>
		</ul>

	</div>


</div>
<?php include('pie.php'); ?>

		