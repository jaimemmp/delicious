<?php
/*
* enviar_link.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('clases/Usuario.php');

//capturo los datos del formulario
$id_link = $_POST["id"];
$usuarios = $_POST["usuarios"];

//si no esta establecido o esta vacio $_POST['usuarios'] pinta el error
if(!isset($_POST["usuarios"]) OR trim($_POST["usuarios"])==""){
	$error = utf8_decode('No has introducido ningún usuario para compartir tu link.');
	header('location: compartir_link.php?id='.$id_link.'&error='.$error);
}

//convierte el array de nicks en una cadena separada por comas
$array_de_usuarios = explode(',', $usuarios);

//comparte el link con los usuarios y guarda el informe en el array informe
$usuario = new Usuario($_SESSION["id"]);
$informe = $usuario->compartirLink($id_link, $array_de_usuarios);

//establece arrays para separar los datos
$correctos = array();
$erroneos = array();

//recorre el informe
foreach($informe as $nick => $valor){
	
	//si el valor es una cadena lo mete en los erroneos
	if(is_string($valor)){
		$erroneos[] = $nick;
	//si no es cadena (será el id insertado) lo mete en los correctos
	}else{
		$correctos[] = $nick;
	}
}

//convierte los nombres correctos e incorectos de arrays a cadenas
$txt_correctos = implode(', ', $correctos);
$txt_erroneos = implode(', ', $erroneos);

//cuenta y guarda las cantidades
$num_correctos = count($correctos);
$num_erroneos = count($erroneos);

//establece las variables de error o acierto
$_SESSION["success"] = utf8_decode('Los '.$num_correctos.' siguientes destinatarios han recibido el enlace correctamente: '.$txt_correctos);
$_SESSION["danger"] = utf8_decode('Los '.$num_erroneos.' siguientes destinatarios no han recibido el enlace correctamente: '.$txt_erroneos);

header('location: panel.php');

?>