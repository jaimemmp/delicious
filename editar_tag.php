<?php
/*
* editar_link.php
*/

include('inc/seguridad.php');
include('inc/config.php');
include('inc/funciones.php');
include('clases/Usuario.php');

$usuario = new Usuario($_SESSION["id"]);
$tag = $usuario->cargarTag($_GET["id"]);
?>

		<?php include('cabecera.php'); ?>
		<?php include('menu.php'); ?>
		<div id="wrapper">
			<br/>
			<?php
				echo pintarAlerts();
			?>
			<form class="form-inline" method="post" action="guardar_tag.php">
				<input type="hidden" name="id" value="<?php echo $tag["id"];?>"/>
				<input type="text" name="nombre" value="<?php echo $tag["nombre"];?>">
				<button type="submit" class="btn btn-primary inlinebutton"><i class="icon-tag icon-white"></i> Guardar cambios</button>
			</form>


		</div>
		<?php include('pie.php'); ?>

		