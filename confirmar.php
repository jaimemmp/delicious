<?php

require_once('inc/config.php');
require_once('clases/Usuario.php');
require_once('inc/funciones.php');

@session_start();

$usuario = new Usuario($_GET["id"]);

if( $usuario->confirmar($_GET["token"]) ){
	$_SESSION["success"] = 'Su cuenta ha sido activada.<br/>
							Puede acceder a su cuenta haciendo clic
							<a href="login.php"><strong>aquí</strong></a>';
}else{
	$_SESSION["danger"] = 'No se ha podido activar su cuenta.';
}

header('Location: index.php');

?>